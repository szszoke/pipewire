# Russian translation of pipewire.
# Copyright (C) 2010 pipewire
# This file is distributed under the same license as the pipewire package.
#
# Leonid Kurbatov <llenchikk@rambler.ru>, 2010, 2012.
# Alexander Potashev <aspotashev@gmail.com>, 2014, 2019.
# Victor Ryzhykh <victorr2007@yandex.ru>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: pipewire\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire/-/"
"issues\n"
"POT-Creation-Date: 2021-05-16 13:13+0000\n"
"PO-Revision-Date: 2021-06-30 13:26+0300\n"
"Last-Translator: Alexey Rubtsov <rushills@gmail.com>\n"
"Language-Team: ru\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.0\n"

#: src/daemon/pipewire.c:45
#, c-format
msgid ""
"%s [options]\n"
"  -h, --help                            Show this help\n"
"      --version                         Show version\n"
"  -c, --config                          Load config (Default %s)\n"
msgstr ""
"%s [опции]\n"
"  -h, --help                            Показать справку\n"
"      --version                         Информация о версии\n"
"  -c, --config                          Указать файл конфигурации (По "
"умолчанию %s)\n"

#: src/daemon/pipewire.desktop.in:4
msgid "PipeWire Media System"
msgstr "Мультимедийная система PipeWire"

#: src/daemon/pipewire.desktop.in:5
msgid "Start the PipeWire Media System"
msgstr "Запустить PipeWire"

#: src/examples/media-session/alsa-monitor.c:585
#: spa/plugins/alsa/acp/compat.c:187
msgid "Built-in Audio"
msgstr "Встроенное аудио"

#: src/examples/media-session/alsa-monitor.c:589
#: spa/plugins/alsa/acp/compat.c:192
msgid "Modem"
msgstr "Модем"

#: src/examples/media-session/alsa-monitor.c:598
#: src/modules/module-zeroconf-discover.c:290
msgid "Unknown device"
msgstr "Неизвестное устройство"

#: src/modules/module-protocol-pulse/modules/module-tunnel-sink.c:182
#: src/modules/module-protocol-pulse/modules/module-tunnel-source.c:182
#, c-format
msgid "Tunnel to %s/%s"
msgstr "Туннель на %s/%s"

#: src/modules/module-pulse-tunnel.c:511
#, c-format
msgid "Tunnel for %s@%s"
msgstr "Туннель для %s@%s"

#: src/modules/module-zeroconf-discover.c:302
#, c-format
msgid "%s on %s@%s"
msgstr "%s на %s@%s"

#: src/modules/module-zeroconf-discover.c:306
#, c-format
msgid "%s on %s"
msgstr "%s по %s"

#: src/tools/pw-cat.c:991
#, c-format
msgid ""
"%s [options] <file>\n"
"  -h, --help                            Show this help\n"
"      --version                         Show version\n"
"  -v, --verbose                         Enable verbose operations\n"
"\n"
msgstr ""
"%s [опции] <файл>\n"
"  -h, --help                            Показать справку\n"
"      --version                         Информация о версии\n"
"  -v, --verbose                         Включить показ подробной информации\n"
"\n"

#: src/tools/pw-cat.c:998
#, c-format
msgid ""
"  -R, --remote                          Remote daemon name\n"
"      --media-type                      Set media type (default %s)\n"
"      --media-category                  Set media category (default %s)\n"
"      --media-role                      Set media role (default %s)\n"
"      --target                          Set node target (default %s)\n"
"                                          0 means don't link\n"
"      --latency                         Set node latency (default %s)\n"
"                                          Xunit (unit = s, ms, us, ns)\n"
"                                          or direct samples (256)\n"
"                                          the rate is the one of the source "
"file\n"
"      --list-targets                    List available targets for --target\n"
"\n"
msgstr ""
"  -R, --remote                          Имя удаленного фоновой службы\n"
"      --media-type                      Задать тип мультимедиа (по умолчанию "
"%s)\n"
"      --media-category                  Задать категорию мультимедиа (по "
"умолчанию %s)\n"
"      --media-role                      Задать роль мультимедиа (по "
"умолчанию %s)\n"
"      --target                          Задать цель узла (по умолчанию %s)\n"
"                                          0 значит не связывать\n"
"      --latency                         Задать задежку узла (по умолчанию "
"%s)\n"
"                                          Xединица (единица = s, ms, us, "
"ns)\n"
"                                          or direct samples (256)\n"
"                                          частота такая же как у источника "
"file\n"
"      --list-targets                    Перечислить доступные цели для --"
"target\n"
"\n"

#: src/tools/pw-cat.c:1016
#, c-format
msgid ""
"      --rate                            Sample rate (req. for rec) (default "
"%u)\n"
"      --channels                        Number of channels (req. for rec) "
"(default %u)\n"
"      --channel-map                     Channel map\n"
"                                            one of: \"stereo\", "
"\"surround-51\",... or\n"
"                                            comma separated list of channel "
"names: eg. \"FL,FR\"\n"
"      --format                          Sample format %s (req. for rec) "
"(default %s)\n"
"      --volume                          Stream volume 0-1.0 (default %.3f)\n"
"  -q  --quality                         Resampler quality (0 - 15) (default "
"%d)\n"
"\n"
msgstr ""
"      --rate                            Частота дискретизации (необходимо "
"для записи) (По умолчанию %u)\n"
"      --channels                        Количество каналов (необходимо для "
"записи) (default %u)\n"
"      --channel-map                     Карта каналов\n"
"                                            одно из: \"stereo\", "
"\"surround-51\",... or\n"
"                                            список каналов через запятую "
"names: eg. \"FL,FR\"\n"
"      --format                          Формат %s (необходимо для записи) "
"(default %s)\n"
"      --volume                          Громкость 0-1.0 (по умолчанию %.3f)\n"
"  -q  --quality                         Качество ресэмплера (0 - 15) (по "
"умолчанию %d)\n"
"\n"

#: src/tools/pw-cat.c:1033
msgid ""
"  -p, --playback                        Playback mode\n"
"  -r, --record                          Recording mode\n"
"  -m, --midi                            Midi mode\n"
"\n"
msgstr ""
"  -p, --playback                        Режим проигрывания\n"
"  -r, --record                          Режим записи\n"
"  -m, --midi                            Режим MIDI\n"
"\n"

#: src/tools/pw-cli.c:2959
#, c-format
msgid ""
"%s [options] [command]\n"
"  -h, --help                            Show this help\n"
"      --version                         Show version\n"
"  -d, --daemon                          Start as daemon (Default false)\n"
"  -r, --remote                          Remote daemon name\n"
"\n"
msgstr ""
"%s [опции] [команда]\n"
"  -h, --help                            Показать справку\n"
"      --version                         Информация о версии\n"
"  -d, --daemon                          Запустить в режиме фоновой службы "
"(По умолчанию false)\n"
"  -r, --remote                          Имя удаленного фоновой службы\n"
"\n"

#: spa/plugins/alsa/acp/acp.c:291
msgid "Pro Audio"
msgstr "Pro Audio"

#: spa/plugins/alsa/acp/acp.c:412 spa/plugins/alsa/acp/alsa-mixer.c:4704
#: spa/plugins/bluez5/bluez5-device.c:1020
msgid "Off"
msgstr "Выключено"

#: spa/plugins/alsa/acp/alsa-mixer.c:2709
msgid "Input"
msgstr "Вход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2710
msgid "Docking Station Input"
msgstr "Вход док-станции"

#: spa/plugins/alsa/acp/alsa-mixer.c:2711
msgid "Docking Station Microphone"
msgstr "Микрофон док-станции"

#: spa/plugins/alsa/acp/alsa-mixer.c:2712
msgid "Docking Station Line In"
msgstr "Линейный вход док-станции"

#: spa/plugins/alsa/acp/alsa-mixer.c:2713
#: spa/plugins/alsa/acp/alsa-mixer.c:2804
msgid "Line In"
msgstr "Линейный вход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2714
#: spa/plugins/alsa/acp/alsa-mixer.c:2798
#: spa/plugins/bluez5/bluez5-device.c:1175
msgid "Microphone"
msgstr "Микрофон"

#: spa/plugins/alsa/acp/alsa-mixer.c:2715
#: spa/plugins/alsa/acp/alsa-mixer.c:2799
msgid "Front Microphone"
msgstr "Фронтальный микрофон"

#: spa/plugins/alsa/acp/alsa-mixer.c:2716
#: spa/plugins/alsa/acp/alsa-mixer.c:2800
msgid "Rear Microphone"
msgstr "Тыловой микрофон"

#: spa/plugins/alsa/acp/alsa-mixer.c:2717
msgid "External Microphone"
msgstr "Внешний микрофон"

#: spa/plugins/alsa/acp/alsa-mixer.c:2718
#: spa/plugins/alsa/acp/alsa-mixer.c:2802
msgid "Internal Microphone"
msgstr "Встроенный микрофон"

#: spa/plugins/alsa/acp/alsa-mixer.c:2719
#: spa/plugins/alsa/acp/alsa-mixer.c:2805
msgid "Radio"
msgstr "Радио"

#: spa/plugins/alsa/acp/alsa-mixer.c:2720
#: spa/plugins/alsa/acp/alsa-mixer.c:2806
msgid "Video"
msgstr "Видео"

#: spa/plugins/alsa/acp/alsa-mixer.c:2721
msgid "Automatic Gain Control"
msgstr "Автоматическая регулировка усиления"

#: spa/plugins/alsa/acp/alsa-mixer.c:2722
msgid "No Automatic Gain Control"
msgstr "Нет автоматической регулировки усиления"

#: spa/plugins/alsa/acp/alsa-mixer.c:2723
msgid "Boost"
msgstr "Усиление"

#: spa/plugins/alsa/acp/alsa-mixer.c:2724
msgid "No Boost"
msgstr "Нет усиления"

#: spa/plugins/alsa/acp/alsa-mixer.c:2725
msgid "Amplifier"
msgstr "Усилитель"

#: spa/plugins/alsa/acp/alsa-mixer.c:2726
msgid "No Amplifier"
msgstr "Нет усилителя"

#: spa/plugins/alsa/acp/alsa-mixer.c:2727
msgid "Bass Boost"
msgstr "Усиление басов"

#: spa/plugins/alsa/acp/alsa-mixer.c:2728
msgid "No Bass Boost"
msgstr "Нет усиления басов"

#: spa/plugins/alsa/acp/alsa-mixer.c:2729
#: spa/plugins/bluez5/bluez5-device.c:1180
msgid "Speaker"
msgstr "Динамик"

#: spa/plugins/alsa/acp/alsa-mixer.c:2730
#: spa/plugins/alsa/acp/alsa-mixer.c:2808
msgid "Headphones"
msgstr "Наушники"

#: spa/plugins/alsa/acp/alsa-mixer.c:2797
msgid "Analog Input"
msgstr "Аналоговый вход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2801
msgid "Dock Microphone"
msgstr "Микрофон док-станции"

#: spa/plugins/alsa/acp/alsa-mixer.c:2803
msgid "Headset Microphone"
msgstr "Микрофон гарнитуры"

#: spa/plugins/alsa/acp/alsa-mixer.c:2807
msgid "Analog Output"
msgstr "Аналоговый выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2809
msgid "Headphones 2"
msgstr "Наушники 2"

#: spa/plugins/alsa/acp/alsa-mixer.c:2810
msgid "Headphones Mono Output"
msgstr "Моно-выход наушников"

#: spa/plugins/alsa/acp/alsa-mixer.c:2811
msgid "Line Out"
msgstr "Линейный выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2812
msgid "Analog Mono Output"
msgstr "Аналоговый моно-выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2813
msgid "Speakers"
msgstr "Динамики"

#: spa/plugins/alsa/acp/alsa-mixer.c:2814
msgid "HDMI / DisplayPort"
msgstr "HDMI / DisplayPort"

#: spa/plugins/alsa/acp/alsa-mixer.c:2815
msgid "Digital Output (S/PDIF)"
msgstr "Цифровой выход (S/PDIF)"

#: spa/plugins/alsa/acp/alsa-mixer.c:2816
msgid "Digital Input (S/PDIF)"
msgstr "Цифровой вход (S/PDIF)"

#: spa/plugins/alsa/acp/alsa-mixer.c:2817
msgid "Multichannel Input"
msgstr "Многоканальный вход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2818
msgid "Multichannel Output"
msgstr "Многоканальный выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2819
msgid "Game Output"
msgstr "Игровой выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2820
#: spa/plugins/alsa/acp/alsa-mixer.c:2821
msgid "Chat Output"
msgstr "Разговорный выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2822
msgid "Chat Input"
msgstr "Разговорный выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:2823
msgid "Virtual Surround 7.1"
msgstr "Виртуальный объёмный звук 7.1"

#: spa/plugins/alsa/acp/alsa-mixer.c:4527
msgid "Analog Mono"
msgstr "Аналоговый моно"

#: spa/plugins/alsa/acp/alsa-mixer.c:4528
msgid "Analog Mono (Left)"
msgstr "Аналоговый моно (левый)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4529
msgid "Analog Mono (Right)"
msgstr "Аналоговый моно (правый)"

#. Note: Not translated to "Analog Stereo Input", because the source
#. * name gets "Input" appended to it automatically, so adding "Input"
#. * here would lead to the source name to become "Analog Stereo Input
#. * Input". The same logic applies to analog-stereo-output,
#. * multichannel-input and multichannel-output.
#: spa/plugins/alsa/acp/alsa-mixer.c:4530
#: spa/plugins/alsa/acp/alsa-mixer.c:4538
#: spa/plugins/alsa/acp/alsa-mixer.c:4539
msgid "Analog Stereo"
msgstr "Аналоговый стерео"

#: spa/plugins/alsa/acp/alsa-mixer.c:4531
msgid "Mono"
msgstr "Моно"

#: spa/plugins/alsa/acp/alsa-mixer.c:4532
msgid "Stereo"
msgstr "Стерео"

#: spa/plugins/alsa/acp/alsa-mixer.c:4540
#: spa/plugins/alsa/acp/alsa-mixer.c:4698
#: spa/plugins/bluez5/bluez5-device.c:1165
msgid "Headset"
msgstr "Гарнитура"

#: spa/plugins/alsa/acp/alsa-mixer.c:4541
#: spa/plugins/alsa/acp/alsa-mixer.c:4699
msgid "Speakerphone"
msgstr "Спикерфон"

#: spa/plugins/alsa/acp/alsa-mixer.c:4542
#: spa/plugins/alsa/acp/alsa-mixer.c:4543
msgid "Multichannel"
msgstr "Многоканальный"

#: spa/plugins/alsa/acp/alsa-mixer.c:4544
msgid "Analog Surround 2.1"
msgstr "Аналоговый объёмный 2.1"

#: spa/plugins/alsa/acp/alsa-mixer.c:4545
msgid "Analog Surround 3.0"
msgstr "Аналоговый объёмный 3.0"

#: spa/plugins/alsa/acp/alsa-mixer.c:4546
msgid "Analog Surround 3.1"
msgstr "Аналоговый объёмный 3.1"

#: spa/plugins/alsa/acp/alsa-mixer.c:4547
msgid "Analog Surround 4.0"
msgstr "Аналоговый объёмный 4.0"

#: spa/plugins/alsa/acp/alsa-mixer.c:4548
msgid "Analog Surround 4.1"
msgstr "Аналоговый объёмный 4.1"

#: spa/plugins/alsa/acp/alsa-mixer.c:4549
msgid "Analog Surround 5.0"
msgstr "Аналоговый объёмный 5.0"

#: spa/plugins/alsa/acp/alsa-mixer.c:4550
msgid "Analog Surround 5.1"
msgstr "Аналоговый объёмный 5.1"

#: spa/plugins/alsa/acp/alsa-mixer.c:4551
msgid "Analog Surround 6.0"
msgstr "Аналоговый объёмный 6.0"

#: spa/plugins/alsa/acp/alsa-mixer.c:4552
msgid "Analog Surround 6.1"
msgstr "Аналоговый объёмный 6.1"

#: spa/plugins/alsa/acp/alsa-mixer.c:4553
msgid "Analog Surround 7.0"
msgstr "Аналоговый объёмный 7.0"

#: spa/plugins/alsa/acp/alsa-mixer.c:4554
msgid "Analog Surround 7.1"
msgstr "Аналоговый объёмный 7.1"

#: spa/plugins/alsa/acp/alsa-mixer.c:4555
msgid "Digital Stereo (IEC958)"
msgstr "Цифровой стерео (IEC958)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4556
msgid "Digital Surround 4.0 (IEC958/AC3)"
msgstr "Цифровой объёмный 4.0 (IEC958/AC3)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4557
msgid "Digital Surround 5.1 (IEC958/AC3)"
msgstr "Цифровой объёмный 5.1 (IEC958/AC3)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4558
msgid "Digital Surround 5.1 (IEC958/DTS)"
msgstr "Цифровой объёмный 5.1 (IEC958/DTS)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4559
msgid "Digital Stereo (HDMI)"
msgstr "Цифровой стерео (HDMI)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4560
msgid "Digital Surround 5.1 (HDMI)"
msgstr "Цифровой объёмный 5.1 (HDMI)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4561
msgid "Chat"
msgstr "Чат"

#: spa/plugins/alsa/acp/alsa-mixer.c:4562
msgid "Game"
msgstr "Игра"

#: spa/plugins/alsa/acp/alsa-mixer.c:4696
msgid "Analog Mono Duplex"
msgstr "Аналоговый моно дуплекс"

#: spa/plugins/alsa/acp/alsa-mixer.c:4697
msgid "Analog Stereo Duplex"
msgstr "Аналоговый стерео дуплекс"

#: spa/plugins/alsa/acp/alsa-mixer.c:4700
msgid "Digital Stereo Duplex (IEC958)"
msgstr "Цифровой стерео дуплекс (IEC958)"

#: spa/plugins/alsa/acp/alsa-mixer.c:4701
msgid "Multichannel Duplex"
msgstr "Многоканальный дуплекс"

#: spa/plugins/alsa/acp/alsa-mixer.c:4702
msgid "Stereo Duplex"
msgstr "Стерео дуплекс"

#: spa/plugins/alsa/acp/alsa-mixer.c:4703
msgid "Mono Chat + 7.1 Surround"
msgstr "Моно чат + 7.1 Surround"

#: spa/plugins/alsa/acp/alsa-mixer.c:4806
#, c-format
msgid "%s Output"
msgstr "%s выход"

#: spa/plugins/alsa/acp/alsa-mixer.c:4813
#, c-format
msgid "%s Input"
msgstr "%s вход"

#: spa/plugins/alsa/acp/alsa-util.c:1175 spa/plugins/alsa/acp/alsa-util.c:1269
#, c-format
msgid ""
"snd_pcm_avail() returned a value that is exceptionally large: %lu byte (%lu "
"ms).\n"
"Most likely this is a bug in the ALSA driver '%s'. Please report this issue "
"to the ALSA developers."
msgid_plural ""
"snd_pcm_avail() returned a value that is exceptionally large: %lu bytes (%lu "
"ms).\n"
"Most likely this is a bug in the ALSA driver '%s'. Please report this issue "
"to the ALSA developers."
msgstr[0] ""
"snd_pcm_avail() возвращает значение, которое является исключительно большим: "
"%lu байт (%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."
msgstr[1] ""
"snd_pcm_avail() возвращает значение, которое является исключительно большим: "
"%lu байта (%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."
msgstr[2] ""
"snd_pcm_avail() возвращает значение, которое является исключительно большим: "
"%lu байт (%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."

#: spa/plugins/alsa/acp/alsa-util.c:1241
#, c-format
msgid ""
"snd_pcm_delay() returned a value that is exceptionally large: %li byte (%s"
"%lu ms).\n"
"Most likely this is a bug in the ALSA driver '%s'. Please report this issue "
"to the ALSA developers."
msgid_plural ""
"snd_pcm_delay() returned a value that is exceptionally large: %li bytes (%s"
"%lu ms).\n"
"Most likely this is a bug in the ALSA driver '%s'. Please report this issue "
"to the ALSA developers."
msgstr[0] ""
"snd_pcm_delay() возвращает значение, которое является исключительно большим: "
"%li байт (%s%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."
msgstr[1] ""
"snd_pcm_delay() возвращает значение, которое является исключительно большим: "
"%li байта (%s%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."
msgstr[2] ""
"snd_pcm_delay() возвращает значение, которое является исключительно большим: "
"%li байт (%s%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."

#: spa/plugins/alsa/acp/alsa-util.c:1288
#, c-format
msgid ""
"snd_pcm_avail_delay() returned strange values: delay %lu is less than avail "
"%lu.\n"
"Most likely this is a bug in the ALSA driver '%s'. Please report this issue "
"to the ALSA developers."
msgstr ""
"snd_pcm_avail_delay() возвращает странное значение: задержка %lu меньше "
"доступных %lu.\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."

#: spa/plugins/alsa/acp/alsa-util.c:1331
#, c-format
msgid ""
"snd_pcm_mmap_begin() returned a value that is exceptionally large: %lu byte "
"(%lu ms).\n"
"Most likely this is a bug in the ALSA driver '%s'. Please report this issue "
"to the ALSA developers."
msgid_plural ""
"snd_pcm_mmap_begin() returned a value that is exceptionally large: %lu bytes "
"(%lu ms).\n"
"Most likely this is a bug in the ALSA driver '%s'. Please report this issue "
"to the ALSA developers."
msgstr[0] ""
"snd_pcm_mmap_begin() возвращает значение, которое является исключительно "
"большим: %lu байт (%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."
msgstr[1] ""
"snd_pcm_mmap_begin() возвращает значение, которое является исключительно "
"большим: %lu байта (%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."
msgstr[2] ""
"snd_pcm_mmap_begin() возвращает значение, которое является исключительно "
"большим: %lu байт (%lu мс).\n"
"Вероятно, это ошибка в драйвере ALSA «%s». Пожалуйста, сообщите об этой "
"проблеме разработчикам ALSA."

#: spa/plugins/alsa/acp/channelmap.h:466
msgid "(invalid)"
msgstr "(недействительно)"

#: spa/plugins/bluez5/bluez5-device.c:1030
msgid "Audio Gateway (A2DP Source & HSP/HFP AG)"
msgstr "Адаптер аудиогарнитуры (приёмник A2DP и HSP/HFP AG)"

#: spa/plugins/bluez5/bluez5-device.c:1053
#, c-format
msgid "High Fidelity Playback (A2DP Sink, codec %s)"
msgstr "Воспроизведение высокого качества (приёмник A2DP, кодек %s)"

#: spa/plugins/bluez5/bluez5-device.c:1055
#, c-format
msgid "High Fidelity Duplex (A2DP Source/Sink, codec %s)"
msgstr "Дуплекс высокого качества (источник / приёмник A2DP,  кодек %s)"

#: spa/plugins/bluez5/bluez5-device.c:1061
msgid "High Fidelity Playback (A2DP Sink)"
msgstr "Воспроизведение высокого качества (приёмник A2DP)"

#: spa/plugins/bluez5/bluez5-device.c:1063
msgid "High Fidelity Duplex (A2DP Source/Sink)"
msgstr "Дуплекс высокого качества (источник / приёмник A2DP)"

#: spa/plugins/bluez5/bluez5-device.c:1090
#, c-format
msgid "Headset Head Unit (HSP/HFP, codec %s)"
msgstr "Гарнитура (HSP/HFP, кодек %s)"

#: spa/plugins/bluez5/bluez5-device.c:1094
msgid "Headset Head Unit (HSP/HFP)"
msgstr "Гарнитура (HSP/HFP)"

#: spa/plugins/bluez5/bluez5-device.c:1170
msgid "Handsfree"
msgstr "Hands-Free устройство"

#: spa/plugins/bluez5/bluez5-device.c:1185
msgid "Headphone"
msgstr "Наушники"

#: spa/plugins/bluez5/bluez5-device.c:1190
msgid "Portable"
msgstr "Портативное устройство"

#: spa/plugins/bluez5/bluez5-device.c:1195
msgid "Car"
msgstr "Автомобильное устройство"

#: spa/plugins/bluez5/bluez5-device.c:1200
msgid "HiFi"
msgstr "Hi-Fi"

#: spa/plugins/bluez5/bluez5-device.c:1205
msgid "Phone"
msgstr "Телефон"

#: spa/plugins/bluez5/bluez5-device.c:1211
msgid "Bluetooth"
msgstr "Bluetooth"
